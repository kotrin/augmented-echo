require 'bundler'

Bundler.require

require File.dirname(__FILE__) + '/lib/augmented_echo.rb'

run AugmentedEcho::Server.new