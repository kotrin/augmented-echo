require 'rest-client'

module AugmentedEcho
  class Worker
    STUFFS = [
      "Waiting for stuffs to do, brah.",
      "The database is what I search.",
      "Here are words to assure you I am doing stuff and not broken.",
      "Party time once I get something to do."
    ]
    
    def self.run(api_url)
      loop do
        if AugmentedEcho::Message.count > 0
          message = AugmentedEcho::Message.first
          url = api_url + "/api/message.json"
          puts "Sending echo message to: #{url}"
          RestClient.post(url, token: message.token, body: message.body)
          message.destroy
        end
  
        if AugmentedEcho::Handshake.count > 0
          handshake = AugmentedEcho::Handshake.first
          url = api_url + "/api/handshake_approval.json"
          puts "Sending handshake approval to: #{url}"
          RestClient.post(url, token: handshake.token)
          handshake.destroy
        end
        
        i = rand(STUFFS.length)
        print STUFFS[i]
        
        5.times {
          sleep 1
          print "."
        }
        puts
      end
    end
  end
end
