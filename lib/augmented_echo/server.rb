require 'sinatra'
require 'json'
require 'sinatra/json'

module AugmentedEcho
  class Server < Sinatra::Base
    post '/message.json' do
      AugmentedEcho::Message.create(body: params[:body], token: params[:token])
      json message: 'created'
    end

    post '/handshake_request.json' do
      AugmentedEcho::Handshake.create(token: params[:token])
      json handshake: 'created'
    end
  end
end