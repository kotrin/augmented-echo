require 'mongo_mapper'

MongoMapper.connection = Mongo::Connection.new('localhost')
MongoMapper.database = 'mydatabasename'

module AugmentedEcho
  class Message
    include MongoMapper::Document
    key :body, type: String
    key :token, type: String
  end

  class Handshake
    include MongoMapper::Document
    key :token, type: String
  end
end

require_relative './augmented_echo/server.rb'
require_relative './augmented_echo/worker.rb'