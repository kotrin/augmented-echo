# augmented-echo

This is an echo server for [Augmented](https://github.com/kotrin/augmented) clients.
It's really handy when trying to test and develop cool stuffs for [Augmented](https://github.com/kotrin/augmented).

augmented-echo will:

* accept handshake requests
* reply to messages (token does not matter)

## You need this stuff

* Ruby 1.9+
* MongoDB

And most importantly: you want [Augmented](https://github.com/kotrin/augmented).

## This is how you do stuff

Install the gems

    bundle install

Run the stuffs

    bundle exec foreman start

A server should start listening on `http://localhost:3333` and hardcoded to respond to `http://localhost:3000`